export class CreneauDTO {
  id: number;
  dateDebut: Date;
  dateFin: Date;
  isDiplay: boolean;

  constructor(creneau: Partial<Creneau>) {
    Object.assign(this, creneau);
  }
}

export class Creneau {
  id: number;
  dateDebut: Date;
  dateFin: Date;
  isDiplay: boolean;

  constructor(creneauDTO: Partial<CreneauDTO>) {
    Object.assign(this, creneauDTO);
  }
}
