import { Component, OnInit } from '@angular/core';
import { Creneau } from "./model/creneau.model";
import { CalendarService } from "./calendar.service";
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  public days: Date[] = [];
  public plageHoraire: number[] = [6, 22];
  public creneaux: Creneau[] = [];
  public grid: Map<Date, number[]> = new Map();
  private creneauDebut: Date = undefined;
  private creneauFin: Date = undefined;
  private creneauDragging: Creneau = undefined;

  get getColumnStyle(): {} {
    return {
      'grid-template-rows': `repeat(${this.plageHoraire[1] - this.plageHoraire[0]}, 1fr)`
    }
  }

  /**
   * Getter permettant de retourner la barre des horaires et ainsi définir la matrice correspondant aux heures / indexOf
   * pour positioner un créneau au bonne endroit dans la grille
   * @return [6, 7, 8, 9, ...]
   */
  get heures(): number[] {
    const stage: number[] = [];
    for (let i = 0; i < (this.plageHoraire[1] - this.plageHoraire[0]); i++) { // Création des plages horaires
      stage.push(this.plageHoraire[0] + i);
    }
    return stage;
  }

  get today(): Date {
    return new Date();
  }

  constructor(private calendarService: CalendarService) {
  }

  ngOnInit(): void {
    this.days = this.calendarService.initWeek(this.today);
    this.initGrid();
    this.calendarService.getCreneauxByWeek(this.days).pipe(
      tap(res => {
        this.creneaux = res;
      })
    ).subscribe();
  }

  public getCreneauPosition(creneau: Creneau): {} {
    const hours = this.heures;
    const heureDebut = hours.indexOf(creneau.dateDebut.getHours()) + 1;
    const heureFin = hours.indexOf(creneau.dateFin.getHours()) + 1;
    return {
      'grid-row': `${heureDebut > 0 ? heureDebut : 1} / ${heureFin > 0 ? heureFin : hours[hours.length]}`
    }
  }

  public setCreneau(): void {
    if (this.creneauDebut != undefined && this.creneauFin != undefined) {
      this.creneaux.push(
        new Creneau({
          id: undefined,
          dateDebut: this.creneauDebut,
          dateFin: this.creneauFin,
          isDiplay: true
        })
      );
    }
    this.creneauDebut = undefined;
    this.creneauFin = undefined;
  }

  public headerClick(date: Date): void {
    this.mouseDown(date, this.plageHoraire[0]);
    this.mouseUp(date, this.plageHoraire[1]);
  }

  public removeCreneau(creneau: Creneau): void {
    this.creneaux = this.creneaux.filter(el => el !== creneau);
  }

  public mouseDown(date: Date, hour: number) {
    this.creneauDebut = new Date(date.getTime());
    this.creneauDebut.setHours(hour);
  }

  public mouseUp(date: Date, hour: number) {
    this.creneauFin = new Date(date.getTime());
    this.creneauFin.setHours(hour + 1);
    this.setCreneau();
  }

  public getKeys(map: Map<Date, number[]>) {
    return Array.from(map.keys());
  }

  public getValues(key: Date) {
    return Array.from(this.grid.get(key));
  }

  public getPosition(hour: number): {} {
    return {
      'grid-row': `${this.heures.indexOf(hour) + 1} / ${this.heures.indexOf(hour) + 1}`
    }
  }

  public onDrop(date: Date, hour: number) {
    // On récupère les heures initialies du créneau qu'on déplace
    const heureDepart = this.creneauDragging.dateDebut.getHours();
    const heureFin = this.creneauDragging.dateFin.getHours();
    this.removeCreneau(this.creneauDragging);
    this.mouseDown(date, hour);
    this.mouseUp(date, ((hour - 1) + (heureFin - heureDepart)));
    this.creneauDragging = undefined;
  }

  /**
   * Permet d'autoriser la dépose d'un élément
   * @param event
   */
  public allowDrop(event): void {
    event.preventDefault();
  }

  public onDragging(creneau: Creneau): void {
    this.creneauDragging = creneau;
  }

  private initGrid(): void {
    for (let day of this.days) {
      this.grid.set(day, this.heures);
    }
  }
}
