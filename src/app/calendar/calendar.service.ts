import { Injectable } from '@angular/core';
import { Creneau } from "./model/creneau.model";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  constructor() {
  }

  public initWeek(curr: Date): Date[] {
    let week = [];
    for (let i = 1; i <= 7; i++) {
      let first = curr.getDate() - curr.getDay() + i;
      let day = new Date(curr.setDate(first));
      week.push(day)
    }
    return week;
  }

  public getCreneauxByWeek(week: Date[]): Observable<Creneau[]> {
    let result: Creneau[] = [];
    for (let i = 0; i < 2; i++) {
      const dateDebut = this.randomDate(week,6, 21);
      const dateFin = new Date(dateDebut.getTime());
      dateFin.setHours(Math.round(Math.random() * (22 - dateDebut.getHours()) + dateDebut.getHours()));
      result.push(new Creneau({
        id: undefined,
        dateDebut: dateDebut,
        dateFin: dateFin
      }))
    }
    return of(result);
  }

  private randomDate(week: Date[], startHour: number, endHour: number): Date {
    const date = week[Math.round(Math.random() * (week.length - 1))];
    const hour = startHour + Math.random() * (endHour - startHour) | 0;
    date.setHours(hour);
    return date;
  }
}
